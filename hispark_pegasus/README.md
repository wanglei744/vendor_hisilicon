# HiSpark Pegasus <a name="ZH-CN_TOPIC_0000001130275863"></a>

- [HiSpark Pegasus <a name="ZH-CN_TOPIC_0000001130275863"></a>](#hispark-pegasus-)
  - [Introduction <a name="section469617221261"></a>](#introduction-)
  - [Constraints <a name="section12212842173518"></a>](#constraints-)
  - [Hi3861V100 Experiment Development Guide<a name="ZH-CN_TOPIC_0000001130176841"></a>](#hi3861v100-experiment-development-guide)
  - [Hi3861V100 Hardware Introduction<a name="section11660541593"></a>](#hi3861v100-hardware-introduction)
  - [Development environment setup <a name="section11660541593"></a>](#development-environment-setup-)
  - [WiFi_IoT basic control experiment <a name="section11660541593"></a>](#wifi_iot-basic-control-experiment-)
  - [Appendix <a name="section11660541593"></a>](#appendix-)
  - [Related repositories <a name="section641143415335"></a>](#related-repositories-)

## Introduction <a name="section469617221261"></a>

compile framework adaptation, solution reference code and scripts.

## Constraints <a name="section12212842173518"></a>

Support HiSpark\_pegasus (Hi3861V100).

## Hi3861V100 Experiment Development Guide<a name="ZH-CN_TOPIC_0000001130176841"></a>

Preface: Before learning the experiment, please learn some theoretical knowledge, [Go to Theoretical Knowledge Course](http://developer.huawei.com/consumer/cn/training/course/introduction/C101641968823265204)

## Hi3861V100 Hardware Introduction<a name="section11660541593"></a>

- [Hi3861V100 Hardware Introduction](http://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-introduction-hi3861.md)

## Development environment setup <a name="section11660541593"></a>

- The first way (recommended):
     - [Source code acquisition](https://device.harmonyos.com/cn/docs/documentation/guide/sourcecode-acquire-0000001050769927)
     - [Building IDE tool development environment](https://device.harmonyos.com/cn/docs/documentation/guide/ide-install-windows-ubuntu-0000001194073744)
     - [Open project/source code](https://device.harmonyos.com/cn/docs/documentation/guide/open_project-0000001071680043)
     - [Compile the source code of Hi3861V100 development board](https://device.harmonyos.com/cn/docs/documentation/guide/ide-hi3861v100-compile- 0000001192526021)
     - [Hi3861V100 development board programming](https://device.harmonyos.com/cn/docs/documentation/guide/ide-hi3861-upload-0000001051668683)
     - [Run the first program Hello world](http://gitee.com/openharmony/docs/blob/master/en-us/device-dev/quick-start/quickstart-lite-steps-hi3861-helloworld.md )
- The second way (hb way):
     - [Source code acquisition](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-sourcecode-acquire.md)
     - [Build Hi3861V100 development environment](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3861-setting.md)
     - [Compile source code of Hi3861V100 development board](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps -hi3861-building.md)
     - [Hi3861V100 development board burning](http://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3861-burn.md)
     - [Run the first program Hello world](http://gitee.com/openharmony/docs/blob/master/en-us/device-dev/quick-start/quickstart-lite-steps-hi3861-helloworld.md )
     - [Environmental Setup FAQ](http://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-steps-hi3861-faqs.md)


## WiFi_IoT basic control experiment <a name="section11660541593"></a>

- [led experiment](https://gitee.com/openharmony/docs/blob/master/en-us/device-dev/guide/device-wlan-led-control.md)

## Appendix <a name="section11660541593"></a>

- [For more information, please visit the official website](https://www.hisilicon.com/cn/products/smart-iot/ShortRangeWirelessIOT/Hi3861V100)
## Related repositories <a name="section641143415335"></a>

- **vendor_hisilicon**

- [device_soc_hisilicon](https://gitee.com/openharmony/device_soc_hisilicon)

- [device_board_hisilicon](https://gitee.com/openharmony/device_board_hisilicon)

- [third_party_u-boot](https://gitee.com/openharmony/third_party_u-boot)
